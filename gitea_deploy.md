**First iteration:**

In this first iteration, I proceeded with the initial installation and configuration. Here are the details of what was done:

- I performed the initial installation of a service (likely Taiga as per the previous iteration), modifying the listening port to 80 to facilitate public access for testing, accessible via the public IP on that port.

- I installed Docker containers for Gitea and Drone. Initially, Gitea was configured to listen on port 3000, but I modified it to also listen on port 80 for easier access. Meanwhile, Drone continued to listen on port 8000.

- I created a small test project that included a functional pipeline which operated successfully.

**Status of the first iteration:**
- Gitea was accessible and functioning correctly on port 80.
- Drone was also accessible and operating on port 8000.
- I launched a pipeline on Drone and it executed without issues.
