
## Test Gitea with Drone on Azure VM

# Quick Implementation Guide
1. Prepare Azure VM
2. Install Docker and Docker Compose
3. Deploy Gitea
4. Configure Drone runner
5. Create and execute pipeline
6. Monitor in Drone dashboard

### 1. Initial Setup
- Azure VM: Ubuntu, 2 cores, 4 GB RAM
- Installation: Docker, Docker Compose

### 2. Gitea Configuration
- Deployment: Docker container (port 80)
- Configuration: URL, admin setup, database
- Project: Created for runner testing

### 3. Drone Integration
- Deployment: Docker container as a runner
- Configuration: Gitea-Drone credentials
- Access: Registration and dashboard access

### 4. Integration Testing
- Commit on Gitea
- Pipeline execution (.drone.yml)

### 5. Preliminary Conclusions
- Advantages: Lightweight, modular
- Challenges: Dual interface, secret management

### 6. Comparison with GitLab-CE
- Installation: Gitea+Drone lighter
- Pipelines: GitLab more robust
- Jobs: GitLab offers more predefined options

# Final Considerations
- Ideal for small to medium projects
- Requires Docker knowledge
- Flexible and scalable as per needs

